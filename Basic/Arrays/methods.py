import array as arr
data=arr.array('i',[10,20,30,40,10])
print(data)

#buffer_info
print(data.buffer_info())
#append
data.append(100);
print(data)

print(data.buffer_info())

#count(data value)
print(data.count(10))

#extend
data.extend([60,70,80])
print(data)
