#writing String Ways

#way1
friend1="Kanha"
print(friend1)
print(type(friend1))

#way2
friend2='Ashsih'
print(friend2)
print(type(friend2))

#way3
friend3="""rahul"""
print(friend3)
print(type(friend3))

#way4
friend4='''Shashi'''
print(friend4)
print(type(friend4))


#list
proLang=["CPP","C","Java","Python"]
print(proLang)
print(type(proLang))

cric=[18,"Virat",45,"Rohit"]
print(cric)
print(type(cric))


#Tuple

empTuple=("CPP",13.5,"Java",11)
print(empTuple)
print(type(empTuple))


#range

x=range(10)
print(x)
print(type(x))

for i in x:
    print(i)
