def outer():
    def inner1(a,b):
        print("in inner1 fun")
        return a+b
    def inner2(c,d):
        print("in inner2 fun")
        return c*d

    print("in outer fun")
    return inner1,inner2

x=int(input("Enter value of x\n"))
y=int(input("Enter value of y\n"))
retobj=outer()

for func in retobj:
    result = func(x, y)
    print("Result:", result)
