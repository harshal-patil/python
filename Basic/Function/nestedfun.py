def outer():
    def inner1():
        print("in inner1 fun")
    def inner2():
        print("in inner2 fun")

    print("in outer fun")
    inner1()
    inner2()

outer()
