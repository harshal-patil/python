'''#input
#it takes Input as String
a=input("Enter the value of a ")
b=input("Enter the value of b ")
print(a+b)


#parse it into int
a=int(input("Enter the value of a "))
b=int(input("Enter the value of b "))
print(a+b)
'''
#output
x=10
y=20
print(x)
print()
print(y)


#print String
print('core2web')
print("core2web")
print("""core2web""")
print('''core2web''')
print("Incubator"+"python")
print("Incubator","python")
print(3*'python')

#output by seperator

print(x,y)
print(x,y,sep=",")
print(x,y,sep=''';''')
print(x,y,sep='======')

#end

print(x,end=" ")
print(x,end=''' ''')
print(x,end=""" """)
print(x,end='...')
print(y)

#formatted Element

x=10
y=20
print("value  of x= %d"%x)
print("value  of x= %d"%y)

print("value  of x= ",x)
print("value  of x= ",y)

print("value of x = {} and value of y = {}".format(x,y))
