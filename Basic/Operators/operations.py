#arithmatic

x=2
y=5
print(x+y)#addition
print(x-y)#substration
print(x*y)#mult
print(x/y)#div return float
print(x%y)#mod
print(x//y)#return Integer
print(x**y)#2 raise to 5

# relational

print(x<y)#less than
print(x>y)#greather than
print(x<=y)#less than or equalls
print(x>=y)#gretaer than or equals
print(x==y)#equals
print(x!=y)#not Equal to

#logical
a=True
b=False

print(a and b)
print(a or b)
print(not a)
print(not b)

#assignment

x=10
y=5

print(x)
x+=y#internally x=x+y
print(x)

#identity

ListData=[10,20,30,40,50]
x=10
y=20
print(id(x))
print(id(y))
print(x is y)
print(x is not y)


#membership

ListData=[10,20,30,40]
x=10
y=25
print(x in ListData)
print(y in ListData)

#bitwise

x=10
y=20

print(x&y)
print(x|y)
print(~x)
print(~y)
