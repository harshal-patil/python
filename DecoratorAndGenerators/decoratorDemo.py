def decorfun(fun):
    print("in decor Fun")
    def wrapper(* args):
        print("Start wrapper")
        val=fun(* args)
        print("end wrapper")
        return val
    return wrapper;
@decorfun
def normalfun(x,y):
    print("in normal fun")
    return x+y
print(normalfun(10,20))
