def decorFun(func):
    def wrapper():
        print("Start wrapper1")
        func()
        print("end wrapper1")
    return wrapper
def decorRun(func):
    def wrapper():
        print("Start wrapper2")
        func()
        print("end wrapper2")
    return wrapper
@decorFun
@decorRun
def normalFun():
    print("In normal fun")

normalFun()
