class Parent:    
    x=10
    y=20
    @staticmethod
    def dispData():
        print(x)
        print(y)

class Child(Parent):
    x=30
    y=40
    '''@classmethod
    def dispData(cls):
        print(cls.x)
        print(cls.y)
        print("child")'''
obj=Child()
obj.dispData()
